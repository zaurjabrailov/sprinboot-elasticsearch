package az.dbadeveloper.springbootelasticserach.controller;


import az.dbadeveloper.springbootelasticserach.domain.dto.search.SearchRequestDTO;
import az.dbadeveloper.springbootelasticserach.domain.entity.Vehicle;
import az.dbadeveloper.springbootelasticserach.service.VehicleDummyDataService;
import az.dbadeveloper.springbootelasticserach.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/vehicle")
public class VehicleController {

    private final VehicleService vehicleService;
    private final VehicleDummyDataService vehicleDummyDataService;

    @PostMapping
    public void index(@RequestBody final Vehicle vehicle) {
        vehicleService.index(vehicle);
    }

    @PostMapping("/insertdummydata")
    public void insertDummyData() {
        vehicleDummyDataService.insertDummyData();
    }

    @GetMapping("/{id}")
    public Vehicle getById(@PathVariable final String id) {
        return vehicleService.getById(id);
    }

    @PostMapping("/search")
    public List<Vehicle> search(@RequestBody final SearchRequestDTO searchRequestDTO) {
        return vehicleService.search(searchRequestDTO);
    }

    @GetMapping("/search/{date}")
    public List<Vehicle> getAllVehiclesCreatedSince(@PathVariable
                                                    @DateTimeFormat(pattern = "yyyy-MM-dd") final Date date) {
        return vehicleService.getAllVehiclesCreatedSince(date);
    }

    @PostMapping("/searchcreatedsince/{date}")
    public List<Vehicle> searchCreatedSince(@RequestBody final SearchRequestDTO searchRequestDTO,
                                            @PathVariable
                                            @DateTimeFormat(pattern = "yyyy-MM-dd") final Date date) {
        return vehicleService.searchCreatedSince(searchRequestDTO, date);
    }
}
