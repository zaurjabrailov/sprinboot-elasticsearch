package az.dbadeveloper.springbootelasticserach.controller.index;

import az.dbadeveloper.springbootelasticserach.service.IndexService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/index")
public class IndexController {

    private final IndexService indexService;

    @PostMapping("/recreate")
    public void recreateAllIndices(){
        indexService.recreateIndices(true);
    }

}
