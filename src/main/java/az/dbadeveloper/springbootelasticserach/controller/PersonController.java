package az.dbadeveloper.springbootelasticserach.controller;

import az.dbadeveloper.springbootelasticserach.domain.entity.Person;
import az.dbadeveloper.springbootelasticserach.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.nio.file.attribute.UserPrincipalNotFoundException;


@RestController
@RequiredArgsConstructor
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    @PostMapping
    public void save(@RequestBody final Person person){
        personService.save(person);
    }

    @GetMapping("/{id}")
    public Person findById(@PathVariable final String id) throws UserPrincipalNotFoundException {
        return personService.findById(id);
    }
}
