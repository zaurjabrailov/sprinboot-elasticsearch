package az.dbadeveloper.springbootelasticserach.domain.repository.esrepository;

import az.dbadeveloper.springbootelasticserach.domain.entity.Person;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface PersonRepositoryES extends ElasticsearchRepository<Person, String> {
}
