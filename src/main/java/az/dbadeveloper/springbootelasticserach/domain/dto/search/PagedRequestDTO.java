package az.dbadeveloper.springbootelasticserach.domain.dto.search;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;

//@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagedRequestDTO {

    private static final int DEFAULT_SIZE = 100;

    @Getter
    private int page;

    private int size;
    public int getSize() {
        return size != 0 ? size : DEFAULT_SIZE;
    }


}
