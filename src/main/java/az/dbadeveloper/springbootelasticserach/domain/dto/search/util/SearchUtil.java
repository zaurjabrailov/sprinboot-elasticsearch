package az.dbadeveloper.springbootelasticserach.domain.dto.search.util;

import az.dbadeveloper.springbootelasticserach.domain.dto.search.SearchRequestDTO;
import lombok.NoArgsConstructor;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
public final class SearchUtil {

    public static SearchRequest buildSearchRequest(final String indexName,
                                                   final SearchRequestDTO searchRequestDTO) {
        try {
            final int page = searchRequestDTO.getPage();
            final int size = searchRequestDTO.getSize();
            final int from = page <= 0 ? 0 : page * size;

            SearchSourceBuilder builder = new SearchSourceBuilder()
                    .from(from) //10 page var
                    .size(size) //ve her page 100 row olacaq 100 defaul vermishikse ona gore
                    .postFilter(getQueryBuilder(searchRequestDTO));

            if (searchRequestDTO.getSortByField() != null) {
                builder = builder.sort(
                        searchRequestDTO.getSortByField(),
                        searchRequestDTO.getOrderBySort() != null ? searchRequestDTO.getOrderBySort() : SortOrder.ASC);
            }

            SearchRequest request = new SearchRequest(indexName);
            request.source(builder);

            return request;
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static SearchRequest buildSearchRequest(final String indexName,
                                                   final String field,
                                                   final Date date) {
        try {
            final SearchSourceBuilder builder = new SearchSourceBuilder()
                    .postFilter(getQueryBuilder(field, date));

            final SearchRequest request = new SearchRequest(indexName);
            request.source(builder);
            return request;

        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static SearchRequest buildSearchRequest(final String indexName,
                                                   final SearchRequestDTO searchRequestDTO,
                                                   final Date date) {
        try {
            final QueryBuilder searchQuery = getQueryBuilder(searchRequestDTO);
            final QueryBuilder dateQuery = getQueryBuilder("created", date);

            final BoolQueryBuilder boolQueryMust = QueryBuilders.boolQuery()
                    .must(searchQuery)
                    .must(dateQuery);

            SearchSourceBuilder builder = new SearchSourceBuilder()
                    .postFilter(boolQueryMust);

            if (searchRequestDTO.getSortByField() != null) {
                builder = builder.sort(
                        searchRequestDTO.getSortByField(),
                        searchRequestDTO.getOrderBySort() != null ? searchRequestDTO.getOrderBySort() : SortOrder.ASC);
            }

            SearchRequest request = new SearchRequest(indexName);
            request.source(builder);

            return request;
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public static QueryBuilder getQueryBuilder(final SearchRequestDTO searchRequestDTO) {
        if (searchRequestDTO == null) {
            return null;
        }

        final List<String> fields = searchRequestDTO.getFields();
        if (CollectionUtils.isEmpty(fields)) {
            return null;
        }

        if (fields.size() > 1) {
            MultiMatchQueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(searchRequestDTO.getSearchTerm())
                    .type(MultiMatchQueryBuilder.Type.CROSS_FIELDS)
                    .operator(Operator.AND);

            fields.forEach(queryBuilder::field);
            return queryBuilder;
        }

        return fields.stream()
                .findFirst()
                .map(field -> QueryBuilders.matchQuery(field, searchRequestDTO.getSearchTerm())
                        .operator(Operator.AND))
                .orElse(null);
    }


    public static QueryBuilder getQueryBuilder(final String field, final Date date) {
        return QueryBuilders.rangeQuery(field).gte(date);//grater than equals

    }
}
