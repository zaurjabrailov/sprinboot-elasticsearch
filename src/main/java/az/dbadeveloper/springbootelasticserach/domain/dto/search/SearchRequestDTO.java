package az.dbadeveloper.springbootelasticserach.domain.dto.search;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.elasticsearch.search.sort.SortOrder;


import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchRequestDTO extends PagedRequestDTO{
    private List<String> fields;
    private String searchTerm;
    private String sortByField;
    private SortOrder orderBySort;

}
