package az.dbadeveloper.springbootelasticserach.service;

import az.dbadeveloper.springbootelasticserach.domain.entity.Vehicle;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Service
@RequiredArgsConstructor
public class VehicleDummyDataService {

    private static final Logger LOG = LoggerFactory.getLogger(VehicleDummyDataService.class);
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private final VehicleService vehicleService;

    public void insertDummyData() {
        vehicleService.index(buildVehicle("11","Audi A1", "AAA-123", "2005-01-01", 506789009L));
        vehicleService.index(buildVehicle("12","Audi A2", "AAB-123", "2021-01-01", 506789009L));
        vehicleService.index(buildVehicle("13","Audi A3", "AAC-123", "2012-01-01", 506789009L));
        vehicleService.index(buildVehicle("14","BMW M1", "BBB-123", "2000-01-01", 506789009L));
        vehicleService.index(buildVehicle("15","BMW M2", "BBA-123", "2014-01-01", 506789009L));
        vehicleService.index(buildVehicle("16","BMW X5", "BBC-123", "2015-01-01", 506789009L));
        vehicleService.index(buildVehicle("17","BMW X7", "CCA-123", "2010-01-01", 506789009L));
        vehicleService.index(buildVehicle("18","VW Golf", "CCB-123", "1977-01-01", 506789009L));
        vehicleService.index(buildVehicle("19","VW Passat", "CCC-123", "1850-01-01", 506789009L));
        vehicleService.index(buildVehicle("20","Skoda Yeti", "DDA-123", "1999-01-01", 506789009L));
    }

    private static Vehicle buildVehicle(final String id,
                                        final String name,
                                        final String number,
                                        final String date,
                                        final Long phoneNumber){
        Vehicle vehicle = new Vehicle();
        vehicle.setId(id);
        vehicle.setName(name);
        vehicle.setNumber(number);
        vehicle.setPhoneNumber(phoneNumber);
        try {
            vehicle.setCreated(DATE_FORMAT.parse(date));
        } catch (ParseException e) {
            LOG.error(e.getMessage(), e);
        }

        return vehicle;

    }
}
