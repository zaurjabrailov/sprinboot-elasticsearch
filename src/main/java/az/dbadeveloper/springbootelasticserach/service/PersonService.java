package az.dbadeveloper.springbootelasticserach.service;

import az.dbadeveloper.springbootelasticserach.domain.entity.Person;
import az.dbadeveloper.springbootelasticserach.domain.repository.esrepository.PersonRepositoryES;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PersonService {

    private final PersonRepositoryES personRepositoryES;

    public void save(final Person person){
        personRepositoryES.save(person);
    }

    public Person findById(final String id) throws UserPrincipalNotFoundException {
        return personRepositoryES.findById(id).
                orElseThrow(
                        () -> new IllegalArgumentException(
                                String.format("User %s was not found in the database", id)));
    }
}
